<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property integer $id
 * @property integer $user_id
 * @property string $full_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $second_last_name
 * @property string $gender
 * @property string $birth
 * @property string $marital_status
 * @property string $spouse_name
 * @property string $email
 * @property string $identification
 * @property string $phone
 * @property string $cellphone
 * @property string $ocupation
 * @property string $address
 * @property string $city
 * @property string $registration_date
 *
 * The followings are the available model relations:
 * @property Agent $agent
 * @property Doctor $doctor
 * @property User $user
 * @property Supervisor $supervisor
 */
class Person extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('full_name, registration_date, gender, marital_status, birth', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('full_name, first_name, middle_name, last_name, second_last_name, spouse_name, email, ocupation, address, city', 'length', 'max'=>254),
			array('gender, phone', 'length', 'max'=>9),                        
			array('marital_status', 'length', 'max'=>11),
			array('identification', 'length', 'max'=>20),
			array('cellphone', 'length', 'max'=>10),
			array('birth', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, full_name, first_name, middle_name, last_name, second_last_name, gender, birth, marital_status, spouse_name, email, identification, phone, cellphone, ocupation, address, city, registration_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agent' => array(self::HAS_ONE, 'Agent', 'id'),
			'doctor' => array(self::HAS_ONE, 'Doctor', 'id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'supervisor' => array(self::HAS_ONE, 'Supervisor', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'full_name' => 'Nombre Completo',
			'first_name' => 'Primer Nombre',
			'middle_name' => 'Segundo Nombre',
			'last_name' => 'Apellido Paterno',
			'second_last_name' => 'Apellido Materno',
			'gender' => 'Género',
			'birth' => 'Fecha Nacimiento',
			'marital_status' => 'Estado Civil',
			'spouse_name' => 'Nombre Conyuge',
			'email' => 'Email',
			'identification' => 'Cédula',
			'phone' => 'Teléfono',
			'cellphone' => 'Celular',
			'ocupation' => 'Ocupación',
			'address' => 'Dirección',
			'city' => 'Ciudad',
			'registration_date' => 'Fecha Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('second_last_name',$this->second_last_name,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('birth',$this->birth,true);
		$criteria->compare('marital_status',$this->marital_status,true);
		$criteria->compare('spouse_name',$this->spouse_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('identification',$this->identification,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('cellphone',$this->cellphone,true);
		$criteria->compare('ocupation',$this->ocupation,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('registration_date',$this->registration_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}