<?php

/**
 * This is the model class for table "doctor".
 *
 * The followings are the available columns in table 'doctor':
 * @property integer $id
 * @property integer $specialty_id
 * @property string $audit
 * @property string $lider_file
 *
 * The followings are the available model relations:
 * @property Agent[] $agents
 * @property Specialty $specialty
 * @property Person $id0
 */
class Doctor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, specialty_id', 'required'),
			array('id, specialty_id', 'numerical', 'integerOnly'=>true),
			array('audit, lider_file', 'length', 'max'=>254),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, specialty_id, audit, lider_file', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agents' => array(self::MANY_MANY, 'Agent', 'agent_doctor(doctor_id, agent_id)'),
			'specialty' => array(self::BELONGS_TO, 'Specialty', 'specialty_id'),
			'id0' => array(self::BELONGS_TO, 'Person', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'specialty_id' => 'Especialidad',
			'audit' => 'Audit',
			'lider_file' => 'Lider File',
                        'full_name' => 'Nombre Completo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('specialty_id',$this->specialty_id);
		$criteria->compare('audit',$this->audit,true);
		$criteria->compare('lider_file',$this->lider_file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}