<?php
/* @var $this DoctorController */
/* @var $model Doctor */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'doctor-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>		
                <?php 
                        $criteria = new CDbCriteria;                        
                        $criteria->order = 'full_name ASC';
                        echo $form->dropDownList($model,'id',CHtml::listData(Person::model()->findAll($criteria),'id','full_name'),array('prompt'=>'Selecciona una persona')); 
                ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'specialty_id'); ?>		
                <?php echo $form->dropDownList($model,'specialty_id',CHtml::listData(Specialty::model()->findAll(),'id','name'),array('prompt'=>'Selecciona una especialidad')); ?>
		<?php echo $form->error($model,'specialty_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'audit'); ?>
		<?php echo $form->textField($model,'audit',array('size'=>60,'maxlength'=>254)); ?>
		<?php echo $form->error($model,'audit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lider_file'); ?>
		<?php echo $form->textField($model,'lider_file',array('size'=>60,'maxlength'=>254)); ?>
		<?php echo $form->error($model,'lider_file'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->