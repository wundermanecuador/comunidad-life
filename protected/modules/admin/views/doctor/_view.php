<?php
/* @var $this DoctorController */
/* @var $data Doctor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->id0->getAttributeLabel('full_name')); ?>:</b>
	<?php echo CHtml::encode($data->id0->full_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('specialty_id')); ?>:</b>
	<?php echo CHtml::encode($data->specialty->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audit')); ?>:</b>
	<?php echo CHtml::encode($data->audit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lider_file')); ?>:</b>
	<?php echo CHtml::encode($data->lider_file); ?>
	<br />


</div>